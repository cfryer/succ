package audio

import (
	"fmt"

	"github.com/mafik/pulseaudio"
)

type AudioOutputs struct {
	Outputs []pulseaudio.Output
	Active  int
}

func GetVolumePercent(pa *pulseaudio.Client) (float64, error) {

	vol, err := pa.Volume()
	if err != nil {
		return 0, fmt.Errorf("error getting current volume: %s", err)
	}
	return float64(vol), nil

}

func SetVolume(pa *pulseaudio.Client, p float64) error {
	err := pa.SetVolume(float32(p))
	if err != nil {
		return fmt.Errorf("error setting volume: %s", err)
	}
	return nil
}

func GetAudioOutputs(pa *pulseaudio.Client) (*AudioOutputs, error) {

	audio := AudioOutputs{}
	var err error
	audio.Outputs, audio.Active, err = pa.Outputs()
	if err != nil {
		return nil, fmt.Errorf("error getting audio outputs: %s", err)
	}

	return &audio, nil
}
