package network

import (
	"fmt"
	"os"
	"sort"
	"strings"
	"time"

	gonet "github.com/Wifx/gonetworkmanager"
	"github.com/godbus/dbus/v5"
	"github.com/google/uuid"
)

type WifiAccess struct {
	Name         string
	Point        gonet.AccessPoint
	Strength     uint8
	Secure       bool
	SecurityType string
}

func removeDuplicatesAccess(accessSlice *[]WifiAccess) []WifiAccess {
	keys := make(map[string]bool)
	list := []WifiAccess{}

	for _, v := range *accessSlice {
		if _, value := keys[v.Name]; !value {
			keys[v.Name] = true
			if v.Name != "" {
				list = append(list, v)
			}
		}
	}

	return list
}

func removeZeroBitrate(accessSlice *[]WifiAccess) []WifiAccess {
	list := []WifiAccess{}
	for _, v := range *accessSlice {
		p := v.Point
		b, err := p.GetPropertyMaxBitrate()
		if err != nil {
			panic(err)
		}
		if b > 0 {
			list = append(list, v)
		}
	}

	return list
}

func GetWirelessDeviceFromString(iface string) (gonet.DeviceWireless, error) {
	nm, err := gonet.NewNetworkManager()
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	d, _ := nm.GetDeviceByIpIface(iface)
	wd, err := gonet.NewDeviceWireless(d.GetPath())
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	return wd, nil
}

func GetAccessPoints(iface string) *[]WifiAccess {

	wd, err := GetWirelessDeviceFromString(iface)
	if err != nil {
		panic(err)
	}

	_ = wd.RequestScan()

	start := time.Now()
	updated := watchForScan(wd.GetPath())
	for {
		if updated || time.Since(start) > (time.Second*6) {
			break
		}
	}

	ap, _ := wd.GetAllAccessPoints()

	var accessPoints []WifiAccess
	for _, p := range ap {
		x, _ := p.GetPropertySSID()
		y, _ := p.GetPropertyStrength()

		secure, secType := parseSecurity(p)
		accessPoints = append(accessPoints,
			WifiAccess{Name: x, Point: p, Strength: y, Secure: secure, SecurityType: secType})
	}

	sort.Slice(accessPoints, func(i, j int) bool {
		return accessPoints[i].Strength > accessPoints[j].Strength
	})

	displayList := removeDuplicatesAccess(&accessPoints)
	displayList = removeZeroBitrate(&displayList)

	return &displayList
}

func watchForScan(path dbus.ObjectPath) bool {
	conn, err := dbus.ConnectSystemBus()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Failed to connect to system bus:", err)
		os.Exit(1)
	}
	defer conn.Close()

	if err = conn.AddMatchSignal(
		dbus.WithMatchObjectPath(path),
		dbus.WithMatchInterface("org.freedesktop.DBus.Properties"),
		dbus.WithMatchSender("org.freedesktop.NetworkManager"),
	); err != nil {
		panic(err)
	}

	c := make(chan *dbus.Signal, 10)
	conn.Signal(c)
	for v := range c {
		s := fmt.Sprint(v.Body)
		done := strings.Contains(s, "LastScan")
		if done {
			return true
		}
	}

	return false
}

func watchForConnectivity() bool {
	conn, err := dbus.ConnectSystemBus()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Failed to connect to system bus:", err)
		os.Exit(1)
	}
	defer conn.Close()

	if err = conn.AddMatchSignal(
		dbus.WithMatchObjectPath("/org/freedesktop/NetworkManager"),
		dbus.WithMatchInterface("org.freedesktop.DBus.Properties"),
		dbus.WithMatchSender("org.freedesktop.NetworkManager"),
	); err != nil {
		panic(err)
	}

	c := make(chan *dbus.Signal, 10)
	conn.Signal(c)
	for v := range c {
		s := fmt.Sprint(v.Body)
		done := strings.Contains(s, "Connectivity")
		if done {
			return true
		}
	}

	return false
}

func ConnectWiFiToOpenAccessPoint(iface string, ssid string, ap gonet.AccessPoint, secure bool, secType string, auto bool) {
	nm, err := gonet.NewNetworkManager()
	if err != nil {
		//
	}
	d, _ := nm.GetDeviceByIpIface(iface)
	wd, err := gonet.NewDeviceWireless(d.GetPath())
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	saved := checkForSavedConnections(iface, ssid)
	killActiveConnections(nm)
	if saved != nil {
		nm.ActivateWirelessConnection(saved, wd, ap)
	} else {
		conn := make(map[string]map[string]interface{})
		//connection section --------------------------------------------
		conn["connection"] = make(map[string]interface{})
		conn["connection"]["id"] = ssid
		conn["connection"]["interface-name"] = iface
		conn["connection"]["type"] = "802-11-wireless"
		conn["connection"]["autoconnect"] = auto
		connectionUUID, err := uuid.NewUUID()
		if err != nil {
			panic(err)
		}
		conn["connection"]["uuid"] = connectionUUID

		// 802-11-wireless section -------------------------------------
		conn["802-11-wireless"] = make(map[string]interface{})
		conn["802-11-wireless"]["mode"] = "infrastructure"
		conn["802-11-wireless"]["mac-address"] = "" //TODO
		conn["802-11-wireless"]["ssid"] = []uint8(ssid)

		// ipv4 section ------------------------------------------------
		conn["ipv4"] = make(map[string]interface{})

		// ipv6 section ------------------------------------------------
		conn["ipv6"] = make(map[string]interface{})
		conn["ipv6"]["may-fail"] = true

		// proxy section -----------------------------------------------
		conn["proxy"] = make(map[string]interface{})

		_, err = nm.AddAndActivateWirelessConnection(conn, wd, ap)
		if err != nil {
			panic(err)
		}
		fmt.Println(conn)
	}
}

func ConnectWiFiToSecureAccessPoint(iface string, ssid string, ap gonet.AccessPoint, secure bool, secType string, auto bool, password string) {
	nm, err := gonet.NewNetworkManager()
	if err != nil {
		//
	}
	d, _ := nm.GetDeviceByIpIface(iface)
	wd, err := gonet.NewDeviceWireless(d.GetPath())
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	saved := checkForSavedConnections(iface, ssid)
	killActiveConnections(nm)
	if saved != nil {
		nm.ActivateWirelessConnection(saved, wd, ap)
	} else {
		conn := make(map[string]map[string]interface{})
		//connection section --------------------------------------------
		conn["connection"] = make(map[string]interface{})
		conn["connection"]["id"] = ssid
		conn["connection"]["interface-name"] = iface
		conn["connection"]["type"] = "802-11-wireless"
		conn["connection"]["autoconnect"] = auto
		connectionUUID, err := uuid.NewUUID()
		if err != nil {
			panic(err)
		}
		conn["connection"]["uuid"] = connectionUUID

		// 802-11-wireless section -------------------------------------
		conn["802-11-wireless"] = make(map[string]interface{})
		conn["802-11-wireless"]["mode"] = "infrastructure"
		conn["802-11-wireless"]["mac-address"] = "" //TODO
		conn["802-11-wireless"]["ssid"] = []uint8(ssid)

		// ipv4 section ------------------------------------------------
		conn["ipv4"] = make(map[string]interface{})

		// ipv6 section ------------------------------------------------
		conn["ipv6"] = make(map[string]interface{})
		conn["ipv6"]["may-fail"] = true

		// proxy section -----------------------------------------------
		conn["proxy"] = make(map[string]interface{})

		if secure {

			conn["802-11-wireless"]["security"] = "802-11-wireless-security"

			// 802-11-wireless-security section -----------------------
			conn["802-11-wireless-security"] = make(map[string]interface{})

			switch secType {
			case "WEP":
				conn["802-11-wireless-security"]["key-mgmt"] = "None"
				conn["802-11-wireless-security"]["wep-key-type"] = uint32(1)
				conn["802-11-wireless-security"]["wep-key0"] = password
			case "WPA1", "WPA2":
				conn["802-11-wireless-security"]["key-mgmt"] = "wpa-psk"
				conn["802-11-wireless-security"]["auth-alg"] = "open"
				conn["802-11-wireless-security"]["psk"] = password
			}
		}
		fmt.Println(conn)
		nm.AddAndActivateWirelessConnection(conn, wd, ap)
	}
}

func killActiveConnections(nm gonet.NetworkManager) {
	active, err := nm.GetPropertyActiveConnections()
	if err != nil {
		panic(err)
	}
	for _, v := range active {
		nm.DeactivateConnection(v)
	}
}

func checkForSavedConnections(iface string, ssid string) gonet.Connection {
	s, _ := gonet.NewSettings()
	active, _ := s.ListConnections()
	for _, v := range active {
		connSettings, _ := v.GetSettings()
		currentConnectionSection := connSettings["connection"]
		if currentConnectionSection["id"] == ssid {
			return v
		}
	}
	return nil
}

func parseSecurity(p gonet.AccessPoint) (bool, string) {
	apflags, _ := p.GetPropertyFlags()
	secure := apflags%2 == 1
	wpa, _ := p.GetPropertyWPAFlags()
	rsn, _ := p.GetPropertyRSNFlags()

	var secType string
	if secure {
		if wpa == 0 && rsn == 0 {
			secType = "WEP"
		}
	}
	if wpa != 0 {
		secType = "WPA1"
	}
	if rsn != 0 {
		secType = "WPA2"
	}
	if int(wpa/100) == 2 || int(wpa/100) == 6 || int(rsn/100) == 2 || int(rsn/100) == 6 {
		secType = "802.1X"
	}

	return secure, secType
}

func GetKnownSSIDs() []string {
	s, _ := gonet.NewSettings()
	allConns, _ := s.ListConnections()
	allSSID := []string{}
	for _, c := range allConns {
		a, _ := c.GetSettings()
		t := a["connection"]["type"]
		ssid := a[fmt.Sprint(t)]["ssid"]
		if ssid != nil {
			b, _ := getBytes(ssid)
			allSSID = append(allSSID, string(b))
		}
	}
	return allSSID
}

func getBytes(key interface{}) ([]byte, error) {
	buf, ok := key.([]byte)
	if !ok {
		return nil, fmt.Errorf("ooops, did not work")
	}

	return buf, nil
}

func GetWifiState() (bool, error) {
	nm, err := gonet.NewNetworkManager()
	if err != nil {
		return false, err
	}
	wifi, err := nm.GetPropertyWirelessEnabled()
	return wifi, err
}

func ToggleWifi(b bool) error {
	nm, err := gonet.NewNetworkManager()
	if err != nil {
		return fmt.Errorf("Error creating network manager: %v", err)
	}
	err = nm.SetPropertyWirelessEnabled(b)
	if err != nil {
		return fmt.Errorf("Error setting wireless enabled: %v", err)
	}
	if b {
		_ = watchForConnectivity()
	}

	return nil
}
