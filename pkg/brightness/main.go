package brightness

import (
	"fmt"
	"log"
	"math"
	"os/exec"
	"strconv"
	"strings"
)

func GetBrightnessPercent() (perc float64, err error) {
	g, err := exec.Command("brightnessctl", "get").Output()
	if err != nil {
		log.Fatal(err)
	}
	m, err := exec.Command("brightnessctl", "max").Output()
	if err != nil {
		log.Fatal(err)
	}

	gConv := string(g)
	gConv = strings.TrimSpace(gConv)
	gInt, err := strconv.Atoi(gConv)
	if err != nil {
		log.Fatal(err)
	}

	mConv := string(m)
	mConv = strings.TrimSpace(mConv)
	mInt, err := strconv.Atoi(mConv)
	if err != nil {
		log.Fatal(err)
	}

	perc = float64(gInt) / float64(mInt)

	return perc, err
}

func SetBrightness(perc float64) (err error) {
	m, err := exec.Command("brightnessctl", "max").Output()
	if err != nil {
		return fmt.Errorf("you done goofed: %v", err)
	}

	mConv := string(m)
	mConv = strings.TrimSpace(mConv)
	mInt, err := strconv.Atoi(mConv)
	if err != nil {
		log.Fatal(err)
	}

	val := int(math.Round(perc * float64(mInt)))

	if val > mInt {
		err := exec.Command("brightnessctl", "set", strconv.Itoa(mInt)).Run()
		if err != nil {
			return fmt.Errorf("you done goofed: %v", err)
		}
	} else {
		err := exec.Command("brightnessctl", "set", strconv.Itoa(val)).Run()
		if err != nil {
			return fmt.Errorf("you done goofed: %v", err)
		}

	}

	return nil
}
