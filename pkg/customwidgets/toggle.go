package customwidgets

import (
	"fmt"
	"sync"
	"sync/atomic"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/driver/desktop"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
)

type ToggleAlign int

const (
	minLongSide   = float32(50)
	standardScale = float32(4)
)

const (
	ToggleAlignCenter ToggleAlign = iota
	ToggleAlignLeading
	ToggleAlignTrailing
)

var _ fyne.Focusable = (*Toggle)(nil)

type Toggle struct {
	widget.BaseWidget

	OnTapped func(bool) `json:"-"`

	Alignment ToggleAlign

	State            bool
	hovered, focused bool

	binder basicBinder
}

func NewToggle(state bool, tapped func(bool)) *Toggle {
	toggle := &Toggle{
		State:    state,
		OnTapped: tapped,
	}

	toggle.ExtendBaseWidget(toggle)
	return toggle
}

func NewToggleWithData(data binding.Bool) *Toggle {
	s, err := data.Get()
	if err != nil {
		fyne.LogError("Error getting bind value %s", err)
	}
	toggle := NewToggle(s, nil)
	toggle.Bind(data)

	return toggle

}

// ----------------------------------------------------------------------------
// Widget Interface Functions
// ----------------------------------------------------------------------------
func (t *Toggle) CreateRenderer() fyne.WidgetRenderer {
	t.ExtendBaseWidget(t)
	track := canvas.NewRectangle(theme.PressedColor())
	active := canvas.NewRectangle(theme.FocusColor())
	thumb := &canvas.Circle{
		FillColor:   theme.ForegroundColor(),
		StrokeWidth: 0}

	objects := []fyne.CanvasObject{track, active, thumb}

	toggle := &toggleRenderer{objects, track, active, thumb, t}
	toggle.Refresh()
	return toggle
}

func (t *Toggle) FocusGained() {
	t.focused = true
	t.Refresh()
}

func (t *Toggle) FocusLost() {
	t.focused = false
	t.Refresh()
}

func (t *Toggle) MinSize() fyne.Size {
	t.ExtendBaseWidget(t)
	return t.BaseWidget.MinSize()
}

func (t *Toggle) MouseIn(*desktop.MouseEvent) {
	t.hovered = true
	t.Refresh()
}

func (t *Toggle) MouseMoved(*desktop.MouseEvent) {}

func (t *Toggle) MouseOut() {
	t.hovered = false
	t.Refresh()
}

func (t *Toggle) Tapped(*fyne.PointEvent) {
	t.swapState()
	t.Refresh()

	if t.OnTapped != nil {
		t.OnTapped(t.State)
	}
}

func (t *Toggle) TypedKey(ev *fyne.KeyEvent) {
	if ev.Name == fyne.KeySpace {
		t.Tapped(nil)
	}
}

func (t *Toggle) TypedRune(rune) {}

// ----------------------------------------------------------------------------
// Bind Functions
// ----------------------------------------------------------------------------
func (t *Toggle) Bind(data binding.Bool) {
	t.binder.SetCallback(t.updateFromData)
	t.binder.Bind(data)

	t.OnTapped = func(_ bool) {
		t.binder.CallWithData(t.writeData)
	}
}

func (t *Toggle) Unbind() {
	t.OnTapped = nil
	t.binder.Unbind()
}

func (t *Toggle) SetState(state bool) {
	if state == t.State {
		return
	}

	t.State = state

	if t.OnTapped != nil {
		t.OnTapped(t.State)
	}

	t.Refresh()
}

func (t *Toggle) updateFromData(data binding.DataItem) {
	if data == nil {
		return
	}
	boolSource, ok := data.(binding.Bool)
	if !ok {
		return
	}
	val, err := boolSource.Get()
	if err != nil {
		fyne.LogError("Error getting current data value", err)
		return
	}
	t.SetState(val)
}

func (t *Toggle) writeData(data binding.DataItem) {
	if data == nil {
		return
	}
	boolTarget, ok := data.(binding.Bool)
	if !ok {
		return
	}
	currentValue, err := boolTarget.Get()
	if err != nil {
		return
	}
	if currentValue != t.State {
		err := boolTarget.Set(t.State)
		if err != nil {
			fyne.LogError(fmt.Sprintf("Failed to set binding value to %t", t.State), err)
		}
	}
}

// ----------------------------------------------------------------------------
// Utility functions
// ----------------------------------------------------------------------------
func (t *Toggle) buttonDiameter() float32 {
	return theme.Padding() * standardScale
}

func (t *Toggle) endOffset() float32 {
	return t.buttonDiameter()/2 + theme.Padding()
}

func (t *Toggle) swapState() {
	t.State = !t.State
}

type toggleRenderer struct {
	objects []fyne.CanvasObject
	track   *canvas.Rectangle
	active  *canvas.Rectangle
	thumb   *canvas.Circle
	toggle  *Toggle
}

// ----------------------------------------------------------------------------
// Renderer Interface functions
// ----------------------------------------------------------------------------
func (r *toggleRenderer) Refresh() {
	r.Layout(r.toggle.Size())
	canvas.Refresh(r.toggle)
}

func (r *toggleRenderer) Layout(size fyne.Size) {
	trackWidth := 2 * theme.Padding()
	diameter := r.toggle.buttonDiameter()
	endPad := r.toggle.endOffset()

	var trackPos, activePos, thumbPos fyne.Position
	var trackSize, activeSize fyne.Size

	trackSize = fyne.NewSize(1.1*diameter, trackWidth)
	trackPos = alignedPosition(r.toggle.Alignment, trackSize, size)

	r.track.Move(trackPos)
	r.track.Resize(trackSize)

	activeOffset := r.moveToggle()
	activePos = trackPos
	activeSize = fyne.NewSize(activeOffset-endPad, trackWidth)
	thumbPos = fyne.NewPos(
		(trackPos.X-trackSize.Width)+activeOffset, trackPos.Y-(diameter-trackSize.Height)/2)

	r.active.Move(activePos)
	r.active.Resize(activeSize)

	r.thumb.Move(thumbPos)
	r.thumb.Resize(fyne.NewSize(diameter, diameter))
}

func (r *toggleRenderer) MinSize() fyne.Size {
	s1 := 3*r.toggle.buttonDiameter() + 2*theme.Padding()
	s2 := r.toggle.buttonDiameter() + 2*theme.Padding()

	return fyne.NewSize(s1, s2)
}

func (r *toggleRenderer) Objects() []fyne.CanvasObject {
	return r.objects
}

func (r *toggleRenderer) SetObjects(objects []fyne.CanvasObject) {
	r.objects = objects
}

func (r *toggleRenderer) Destroy() {}

// ----------------------------------------------------------------------------
// Renderer Utility Functions
// ----------------------------------------------------------------------------
func (r *toggleRenderer) moveToggle() float32 {
	endPad := r.toggle.endOffset()
	size := r.track.Size()
	if r.toggle.State == true {
		return size.Width + endPad - theme.Padding()
	} else if r.toggle.State == false {
		return endPad
	}
	return 0
}

func alignedPosition(align ToggleAlign, track, layoutSize fyne.Size) (pos fyne.Position) {
	pos.Y = layoutSize.Height / 2
	switch align {
	case ToggleAlignCenter:
		pos.X = (layoutSize.Width - track.Width) / 2
	case ToggleAlignLeading:
		pos.X = theme.Padding() * 2
	case ToggleAlignTrailing:
		pos.X = layoutSize.Width - track.Width - theme.Padding()*2
	}

	return // named return for pos
}

// ----------------------------------------------------------------------------
// Basic Binder structs (from fyne internal)
// ----------------------------------------------------------------------------
type basicBinder struct {
	callback atomic.Value // func(binding.DataItem)

	dataListenerPairLock sync.RWMutex
	dataListenerPair     annotatedListener // access guarded by dataListenerPairLock
}

// Bind replaces the data item whose changes are tracked by the callback function.
func (binder *basicBinder) Bind(data binding.DataItem) {
	listener := binding.NewDataListener(func() { // NB: listener captures `data` but always calls the up-to-date callback
		f := binder.callback.Load()
		if f != nil {
			f.(func(binding.DataItem))(data)
		}
	})
	data.AddListener(listener)
	listenerInfo := annotatedListener{
		data:     data,
		listener: listener,
	}

	binder.dataListenerPairLock.Lock()
	binder.unbindLocked()
	binder.dataListenerPair = listenerInfo
	binder.dataListenerPairLock.Unlock()
}

// CallWithData passes the currently bound data item as an argument to the
// provided function.
func (binder *basicBinder) CallWithData(f func(data binding.DataItem)) {
	binder.dataListenerPairLock.RLock()
	data := binder.dataListenerPair.data
	binder.dataListenerPairLock.RUnlock()
	f(data)
}

// SetCallback replaces the function to be called when the data changes.
func (binder *basicBinder) SetCallback(f func(data binding.DataItem)) {
	binder.callback.Store(f)
}

// Unbind requests the callback to be no longer called when the previously bound
// data item changes.
func (binder *basicBinder) Unbind() {
	binder.dataListenerPairLock.Lock()
	binder.unbindLocked()
	binder.dataListenerPairLock.Unlock()
}

// unbindLocked expects the caller to hold dataListenerPairLock.
func (binder *basicBinder) unbindLocked() {
	previousListener := binder.dataListenerPair
	binder.dataListenerPair = annotatedListener{nil, nil}

	if previousListener.listener == nil || previousListener.data == nil {
		return
	}
	previousListener.data.RemoveListener(previousListener.listener)
}

type annotatedListener struct {
	data     binding.DataItem
	listener binding.DataListener
}
