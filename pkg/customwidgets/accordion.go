package customwidgets

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
)

const accordionDividerHeight = 1

var _ fyne.Widget = (*AccordionWithIcons)(nil)

type AccordionWithIcons struct {
	widget.BaseWidget
	Items     []*AccordionItemWithIcon
	MultiOpen bool
}

func NewAccordionWithIcons(items ...*AccordionItemWithIcon) *AccordionWithIcons {
	a := &AccordionWithIcons{
		Items: items,
	}
	a.ExtendBaseWidget(a)
	return a
}

func (a *AccordionWithIcons) CreateRenderer() fyne.WidgetRenderer {
	a.ExtendBaseWidget(a)
	r := &accordionWithItemsRenderer{
		container: a,
	}
	r.updateObjects()
	return r
}

type accordionWithItemsRenderer struct {
	objects   []fyne.CanvasObject
	container *AccordionWithIcons
	headers   []*widget.Button
	dividers  []fyne.CanvasObject
}

func (r *accordionWithItemsRenderer) SetObjects(objects []fyne.CanvasObject) {
	r.objects = objects
}

func (r *accordionWithItemsRenderer) Layout(size fyne.Size) {
	x := float32(0)
	y := float32(0)
	for i, ai := range r.container.Items {
		if i != 0 {
			div := r.dividers[i-1]
			div.Move(fyne.NewPos(x, y))
			div.Resize(fyne.NewSize(size.Width, accordionDividerHeight))
			y += accordionDividerHeight
		}

		h := r.headers[i]
		h.Move(fyne.NewPos(x, y))
		min := h.MinSize().Height
		h.Resize(fyne.NewSize(size.Width, min))
		y += min
		if ai.Open {
			y += theme.Padding()
			d := ai.Detail
			d.Move(fyne.NewPos(x, y))
			min := d.MinSize().Height
			d.Resize(fyne.NewSize(size.Width, min))
			y += min
			y += theme.Padding()
		}
	}
}

func (r *accordionWithItemsRenderer) MinSize() (size fyne.Size) {
	for i, ai := range r.container.Items {
		if i != 0 {
			size.Height += accordionDividerHeight
		}
		min := r.headers[i].MinSize()
		size.Width = fyne.Max(size.Width, min.Width)
		size.Height += min.Height
		min = ai.Detail.MinSize()
		size.Width = fyne.Max(size.Width, min.Width)
		if ai.Open {
			size.Height += min.Height
			size.Height += theme.Padding()
		}
	}
	return //returns named return size
}

func (a *AccordionWithIcons) Open(index int) {
	if index < 0 || index >= len(a.Items) {
		return
	}
	for i, ai := range a.Items {
		if i == index {
			ai.Open = true
		} else if !a.MultiOpen {
			ai.Open = false
		}
	}
	a.Refresh()
}

func (a *AccordionWithIcons) Close(index int) {
	if index < 0 || index >= len(a.Items) {
		return
	}
	a.Items[index].Open = false
	a.Refresh()
}

func (r *accordionWithItemsRenderer) Refresh() {
	r.updateObjects()
	r.Layout(r.container.Size())
	canvas.Refresh(r.container)
}

func (r *accordionWithItemsRenderer) Objects() []fyne.CanvasObject {
	return r.objects
}

func (r *accordionWithItemsRenderer) Destroy() {
}

func (r *accordionWithItemsRenderer) updateObjects() {
	is := len(r.container.Items)
	hs := len(r.headers)
	ds := len(r.dividers)
	i := 0
	for ; i < is; i++ {
		ai := r.container.Items[i]
		var h *widget.Button
		if i < hs {
			h = r.headers[i]
			h.Show()
		} else {
			h = &widget.Button{}
			r.headers = append(r.headers, h)
			hs++
		}
		h.Alignment = widget.ButtonAlignLeading
		h.IconPlacement = widget.ButtonIconLeadingText
		h.Hidden = false
		h.Importance = widget.LowImportance
		h.Text = ai.Title
		index := i // capture
		h.OnTapped = func() {
			if ai.Open {
				r.container.Close(index)
			} else {
				r.container.Open(index)
			}
		}
		h.Icon = ai.Icon
		if ai.Open {
			ai.Detail.Show()
		} else {
			ai.Detail.Hide()
		}
		h.Refresh()
	}
	// Hide extras
	for ; i < hs; i++ {
		r.headers[i].Hide()
	}
	// Set objects
	objects := make([]fyne.CanvasObject, hs+is+ds)
	for i, header := range r.headers {
		objects[i] = header
	}
	for i, item := range r.container.Items {
		objects[hs+i] = item.Detail
	}
	// add dividers
	for i = 0; i < ds; i++ {
		if i < len(r.container.Items)-1 {
			r.dividers[i].Show()
		} else {
			r.dividers[i].Hide()
		}
		objects[hs+is+i] = r.dividers[i]
	}
	// make new dividers
	for ; i < is-1; i++ {
		div := widget.NewSeparator()
		r.dividers = append(r.dividers, div)
		objects = append(objects, div)
	}
	r.SetObjects(objects)
}

// Accordion Item with Chosen Icon //

type AccordionItemWithIcon struct {
	Title  string
	Icon   fyne.Resource
	Detail fyne.CanvasObject
	Open   bool
}

func NewAccordionItemWithIcon(title string, icon fyne.Resource, detail fyne.CanvasObject) *AccordionItemWithIcon {
	return &AccordionItemWithIcon{
		Title:  title,
		Detail: detail,
		Icon:   icon,
	}
}
