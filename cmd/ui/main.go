package main

import (
	"fyne.io/fyne/v2/app"
	"gitlab.com/cfryer/succ/cmd/ui/popups"
)

func main() {
	succApp := app.New()
	network := popups.NewNetworkPopup(succApp)
	brightness := popups.NewBrightnessPopup(succApp)
	// audio := popups.NewVolumePopup(succApp)
	network.Show()
	// audio.Show()
	brightness.Show()
	succApp.Run()
	// brightness.Title()
}
