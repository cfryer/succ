package popups

import (
	"fmt"
	"os/exec"
	"strings"

	fyne "fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"github.com/Wifx/gonetworkmanager"
	"gitlab.com/cfryer/succ/pkg/customwidgets"
	"gitlab.com/cfryer/succ/pkg/network"
)

func NewNetworkPopup(app fyne.App) fyne.Window {
	w := app.NewWindow("SUCC - Network")

	//------------------------------------------------------------------------
	// Top section
	//------------------------------------------------------------------------
	wifiState, err := network.GetWifiState()
	if err != nil {
		fyne.LogError("Error getting wifi state", err)
	}
	topColor := &canvas.Rectangle{}
	setTopColor(wifiState, topColor)
	wifiStatus := binding.NewBool()
	wifiStatus.Set(wifiState)

	wifiToggle := customwidgets.NewToggleWithData(wifiStatus)
	wifiToggle.Alignment = customwidgets.ToggleAlignTrailing
	wifiToggle.OnTapped = func(b bool) {
		network.ToggleWifi(b)
		setTopColor(b, topColor)
	}

	wifiForm := widget.NewForm(
		widget.NewFormItem("Use Wi-Fi", wifiToggle))

	extraPadding := &canvas.Rectangle{
		FillColor:   theme.BackgroundColor(),
		StrokeColor: theme.BackgroundColor(),
	}
	extraPadding.Resize(fyne.NewSize(1, theme.Padding()))

	topContent := container.NewVBox(
		wifiForm,
		extraPadding,
		extraPadding)

	topSection := container.NewMax(topColor, topContent)
	//------------------------------------------------------------------------
	// Middle section
	//------------------------------------------------------------------------
	loadingBar := widget.NewProgressBarInfinite()
	loadingBar.Start()
	loadingBar.Show()

	currentNetwork := widget.NewLabel("Current Network")
	//TODO
	// currentNetwork := customwidgets.NewAccordionWithIcons()
	// currentNetworkLabel := customwidgets.NewLabelWithStyle()
	// currentNetContainer := container.NewWithoutLayout()

	currentNetContainer := container.NewVBox(widget.NewSeparator(), currentNetwork, loadingBar, widget.NewSeparator())
	otherNetContainer := container.NewVBox(
		widget.NewLabelWithStyle("Searching for Wi-Fi networks...", fyne.TextAlignLeading, fyne.TextStyle{Italic: true}),
		layout.NewSpacer(),
	)

	middleSection := container.NewBorder(
		currentNetContainer,
		nil, nil, nil,
		otherNetContainer,
	)

	//------------------------------------------------------------------------
	// Bottom section
	//------------------------------------------------------------------------
	savedNetworksButton := widget.Button{
		DisableableWidget: widget.DisableableWidget{},
		Text:              "Saved networks",
		Icon:              nil,
		Importance:        widget.LowImportance,
		Alignment:         widget.ButtonAlignLeading,
		IconPlacement:     0,
		OnTapped: func() {
			fmt.Println("open saved network window")
		},
	}
	bottomSection := container.NewVBox(
		widget.NewSeparator(),
		&savedNetworksButton,
	)

	c := container.NewBorder(topSection, bottomSection, nil, nil, middleSection)

	w.SetContent(c)

	w.Resize(fyne.NewSize(750, 750))

	// Updates middle section
	go NewAccessPointAccordion(loadingBar, otherNetContainer)

	return w

}

func setTopColor(b bool, r *canvas.Rectangle) {
	if b {
		r.FillColor = theme.FocusColor()
		r.Refresh()
	} else {
		r.FillColor = theme.DisabledColor()
		r.Refresh()
	}
}

func NewAccessPointAccordion(
	bar *widget.ProgressBarInfinite,
	s *fyne.Container,
) {
	out, err := exec.Command("bash", []string{"-c", "iw dev | grep Interface | cut -d \" \" -f2"}...).Output()
	if err != nil {
		fmt.Println("Error running iw", err)
	}
	dev := strings.TrimSpace(string(out))

	ap := network.GetAccessPoints(dev)

	var listMenu []*customwidgets.AccordionItemWithIcon
	for _, v := range *ap {
		newMenuItem := &customwidgets.AccordionItemWithIcon{
			Title:  v.Name,
			Icon:   nil,
			Detail: nil,
		}
		newMenuItem.Icon = setIcon(newMenuItem, v.Strength, v.Secure)
		newMenuItem.Detail = makeDetail(newMenuItem, dev, v.Point, v.Secure, v.SecurityType, true)
		listMenu = append(listMenu, newMenuItem)
	}

	bar.Hide()

	setMenu := customwidgets.NewAccordionWithIcons(listMenu...)
	scrollContainer := container.NewVScroll(setMenu)
	// scrollContainer.SetMinSize(fyne.NewSize(1,200))

	s.Objects = []fyne.CanvasObject{scrollContainer}
	s.Layout = layout.NewMaxLayout()
	s.Refresh()
}

func makeAction(label string, dev string, ap gonetworkmanager.AccessPoint, secure bool, secType string, auto bool, password string) func() {
	if secure {
		return func() {
			fmt.Printf("attemptign to connect %s to %s\n", dev, label)
			network.ConnectWiFiToSecureAccessPoint(dev, label, ap, secure, secType, auto, password)
		}
	} else {
		return func() {
			fmt.Printf("attemptign to connect %s to %s\n", dev, label)
			network.ConnectWiFiToOpenAccessPoint(dev, label, ap, secure, secType, auto)
		}
	}
}

func setIcon(menuItem *customwidgets.AccordionItemWithIcon, str uint8, secure bool) fyne.Resource {
	if str > 75 {
		if secure {
			return resource4lockPng
		} else {
			return resource4strPng
		}
	} else if str > 50 {
		if secure {
			return resource3lockPng
		} else {
			return resource3strPng
		}
	} else if str > 25 {
		if secure {
			return resource2lockPng
		} else {
			return resource2strPng
		}
	} else {
		if secure {
			return resource1lockPng
		} else {
			return resource1strPng
		}
	}
}

func makeDetail(
	menuItem *customwidgets.AccordionItemWithIcon,
	dev string,
	ap gonetworkmanager.AccessPoint,
	secure bool,
	secType string,
	known bool,
) fyne.CanvasObject {

	checked := binding.NewBool()
	if known {
		checked.Set(true)
	} else {
		if secure {
			checked.Set(true)
		} else {
			checked.Set(false)
		}
	}

	password := widget.NewPasswordEntry()
	p := widget.NewForm(
		widget.NewFormItem("Password", password),
	)
	p.Hide()

	b := widget.Button{
		Text: "Connect",
	}
	a := widget.NewCheckWithData("Auto Connect", checked)

	if secure {
		b.OnTapped = func() { p.Show() }
	} else {
		b.OnTapped = makeAction(menuItem.Title, dev, ap, secure, secType, a.Checked, "")
	}

	p.OnSubmit = func() {
		pass := password.Text
		auto, _ := checked.Get()
		fmt.Println(pass)
		network.ConnectWiFiToSecureAccessPoint(dev, menuItem.Title, ap, secure, secType, auto, pass)
	}

	w := widget.NewForm(
		widget.NewFormItem("", a),
		widget.NewFormItem("", &b),
	)

	c := container.NewVBox(
		w,
		p,
	)

	return c
}
