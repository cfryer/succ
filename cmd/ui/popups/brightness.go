package popups

import (
	"fmt"
	"log"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/cfryer/succ/pkg/brightness"
)

func NewBrightnessPopup(app fyne.App) fyne.Window {
	w := app.NewWindow("SUCC - Brightness")
	w.Resize(fyne.NewSize(75, 350))

	perc, err := brightness.GetBrightnessPercent()
	if err != nil {
		log.Fatal(err)
	}

	perc = 100 * perc

	f := binding.NewFloat()
	short := binding.FloatToStringWithFormat(f, "%0.0f%%")
	f.AddListener(binding.NewDataListener(func() {
		p, err := f.Get()
		if err != nil {
			fmt.Println(err)
		}
		p = p / 100
		err = brightness.SetBrightness(p)
		if err != nil {
			fmt.Println(err)
		}
	}))
	err = f.Set(perc)
	if err != nil {
		log.Fatal(err)
	}

	btclSlider := widget.NewSliderWithData(5.0, 100.0, f)
	btclSlider.Orientation = 1
	w.SetContent(container.NewBorder(
		container.NewCenter(
			container.NewVBox(
				widget.NewLabelWithData(short),
				widget.NewIcon(theme.VisibilityIcon()),
			),
		),
		container.NewCenter(
			container.NewVBox(
				widget.NewIcon(theme.VisibilityOffIcon()),
				widget.NewButton("OK", func() { w.Close() }),
			),
		),
		nil,
		nil,
		btclSlider,
	))

	return w
}
