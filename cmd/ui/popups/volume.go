package popups

import (
	"fmt"
	"log"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"github.com/mafik/pulseaudio"
	"gitlab.com/cfryer/succ/pkg/audio"
)

func NewVolumePopup(app fyne.App) fyne.Window {
	w := app.NewWindow("SUCC - Volume")
	w.Resize(fyne.NewSize(350, 350))

	pa, _ := pulseaudio.NewClient()

	perc, err := audio.GetVolumePercent(pa)
	if err != nil {
		log.Fatal(err)
	}
	audioOutputs, err := audio.GetAudioOutputs(pa)
	if err != nil {
		log.Fatal(err)
	}
	pa.Close()

	perc = 100 * perc

	f := binding.NewFloat()
	short := binding.FloatToStringWithFormat(f, "%0.0f%%")
	f.AddListener(binding.NewDataListener(func() {
		p, err := f.Get()
		if err != nil {
			fmt.Println(err)
		}
		if int(p)%3 == 0 || p == 100 {
			pa, _ = pulseaudio.NewClient()
			err = audio.SetVolume(pa, p/100)
			if err != nil {
				fmt.Println(err)
			}
			pa.Close()
		}
	}))
	err = f.Set(perc)
	if err != nil {
		log.Fatal(err)
	}

	var allAudioOutputsString []string
	var useAudioOutputsString []string
	for _, o := range audioOutputs.Outputs {
		if o.Available || o.PortID == "none" {
			useAudioOutputsString = append(useAudioOutputsString, o.PortName)
		}
		allAudioOutputsString = append(allAudioOutputsString, o.PortName)

	}

	volSlider := widget.NewSliderWithData(0, 100.0, f)
	volSlider.Orientation = 0

	outPicker := widget.NewSelect(useAudioOutputsString, func(input string) {
		pa, _ := pulseaudio.NewClient()
		allOut, _, _ := pa.Outputs()

		audioMap := make(map[string]pulseaudio.Output)
		for i := 0; i < len(allOut); i += 1 {
			audioMap[allOut[i].PortName] = allOut[i]
		}

		audioMap[input].Activate()
	})
	outPicker.SetSelected(allAudioOutputsString[audioOutputs.Active])

	w.SetContent(container.NewBorder(
		container.NewCenter(
			container.NewHBox(
				widget.NewLabelWithData(short),
				widget.NewIcon(theme.VolumeUpIcon()),
				outPicker,
			),
		),
		container.NewCenter(
			container.NewVBox(
				widget.NewIcon(theme.VolumeDownIcon()),
				widget.NewButtonWithIcon("", theme.VolumeMuteIcon(), func() {
					pa, _ := pulseaudio.NewClient()
					defer pa.Close()
					pa.ToggleMute()
				}),
				widget.NewButton("OK", func() { w.Close() }),
			),
		),
		nil,
		nil,
		volSlider,
	))

	return w
}
